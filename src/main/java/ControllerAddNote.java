/*
Контроллер второго окна (создания новой заметки), служит обработчиком объектов, размещенных в макете WindowAddNote.fxml,
там же указан в качестве fx-контроллера
 */
import com.jfoenix.controls.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import java.net.URL;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;
import java.util.ResourceBundle;

public class ControllerAddNote implements Initializable {
   @FXML
   public JFXDatePicker user_date;
   public JFXTimePicker user_time;
   public JFXTextArea text_note;
   public Label length_info;
   public Label length_info_1;
   final private int MAX_LENGTH = 100;
   public Button button_create;
   public JFXComboBox special_status;
    //  public Label txtContent;

   public void initialize(URL location, ResourceBundle resources) {

      user_time.setIs24HourView(true);

      special_status.getItems().addAll("Низкая", "Средняя", "Высокая");
      special_status.setValue("Средняя");

      //Инициализация полей даты-времени
      user_date.setValue(LocalDate.now());
      user_time.setValue(LocalTime.now());
      //Добавим текстовый листнер и каунтер символов
      text_note.textProperty().addListener(new ChangeListener<String>() {
         public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
            int l1 = text_note.getText().length();
            length_info_1.setText(Integer.toString(MAX_LENGTH - l1));
            if (l1>MAX_LENGTH) {
               String s = text_note.getText().substring(0, MAX_LENGTH);
               text_note.setText(s);
            }
         }
      });
      // Устанавливаем обработчик нажатия на кропку button_create

      button_create.setOnAction(new EventHandler<ActionEvent>() {
         @Override
         public void handle(ActionEvent event) {
             String newNoteText = text_note.getText();
             Date newDate = java.sql.Date.valueOf(user_date.getValue());
             Time newTime = java.sql.Time.valueOf(user_time.getValue());
             String newStatus = special_status.getValue().toString();

             Note note = new Note(newNoteText, newDate, newTime, newStatus);
             UseDB useDB = new UseDB();
             useDB.run();
             if (useDB.setNewRecord(note)) {
                 Alert alert = new Alert(Alert.AlertType.INFORMATION);
                 alert.setTitle("Заметка добавлена");
                 alert.setHeaderText(null);
                 alert.setContentText("Новая заметка успешно добвлена");
                 Optional<ButtonType> optBtn= alert.showAndWait();
                 if (optBtn.get() == ButtonType.OK) {
                     System.out.println("Ok!"); // временно для проверки выполнения условия
                 }

             }
         }
      });


}

}