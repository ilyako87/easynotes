import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MainWindow extends Application {

    public static void main(String[] args) {
        launch(args);
        System.out.println("Good bye!");

    }

    @Override
    public void start(Stage stage) throws Exception {
        try {
            String fxmlFile = "MainWindow.fxml";
            FXMLLoader loader = new FXMLLoader();
            Parent root = loader.load(getClass().getResourceAsStream(fxmlFile));
            stage.setTitle("EasyNotes 1.0");
            stage.setScene(new Scene(root));
            stage.show();
            stage.setResizable(false); //запрет изменения размера
        } catch (IOException e) {e.printStackTrace();}


    }
}
