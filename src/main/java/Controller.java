/*
Контроллер осноного окна, служит обработчиком объектов, размещенных в макете MainWindow.fxml,
там же указан в качестве fx-контроллера
 */

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Controller {
    @FXML
    public Button add_new_record;
    public TableView main_table;
    public TableColumn note_column;
    public TableColumn date_column;
    public TableColumn status_column;
    public TableColumn time_column;
    public Button refresh_notes;

    @FXML
    public void initialize() {

        makeTable();

        // Устанавливаем обработчик нажатия на кнопку add_new_record
        add_new_record.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                String fxmlFile = "WindowAddNote.fxml";
                String newTitle = "EasyNotes 1.0: Добавить новую заметку";
                try {
                    Stage newStage = new Stage();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource(fxmlFile));
                    Parent root = loader.load();
                    newStage.setTitle(newTitle);
                    newStage.setScene(new Scene(root));
                    newStage.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        refresh_notes.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                 main_table.setItems(null);
                 makeTable();
            }
        });

    }

    /*Метод заполняет основное View главной Scene (таблицу)
      данными из getNotesList;
      r-set у нас результаты SQL-запроса;
     */
    @FXML
    private void makeTable() {

        UseDB useDB = new UseDB();
        useDB.run();
        ResultSet rSet = useDB.getDataFromDB();

        note_column.setCellValueFactory(new PropertyValueFactory<>("noteText"));
        date_column.setCellValueFactory(new PropertyValueFactory<>("dateOfNote"));
        time_column.setCellValueFactory(new PropertyValueFactory<>("timeOfNote"));
        status_column.setCellValueFactory(new PropertyValueFactory<>("specStatus"));

        ObservableList<Note> list = getNotesList(rSet); //rSet - передаем дальше
        main_table.setItems(list);

        useDB.destroyConnectBD();

    }

    // Метод преобразует данные полученные из SQL-запроса в формате ResultSet в формат, который подходит для TableView
    private ObservableList<Note> getNotesList(ResultSet rSet) {
        ObservableList<Note> list = FXCollections.observableArrayList();
        try {
            while (rSet.next()) {
                //rSet.next();
                Note note = new Note();
                note.setNoteText(rSet.getString(2));
                note.setDateOfNote(rSet.getDate(3));
                note.setTimeOfNote(rSet.getTime(4));
                note.setSpecStatus(rSet.getString(5));
                list.add(note);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }
}
