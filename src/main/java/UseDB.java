/*
Класс UseDB презназначен для работы с базой данных
 */
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.sql.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;

public class UseDB implements Runnable{
    private String hostName;
    private final String tableName = "notes";
    private String portNo;
    private String nameDB;
    private String userName;
    private String userPswd;
    private Connection connect;
    private  final String logFileName = "DB_log.txt";
    private boolean errorDetected = false;
    /* функции для Postgres и MySQL
    private final String postgreMethodSTD1 = "to_date('";
    private final String postgreMethodSTD2 = "', yyyy-mm-dd), '";
    private final String mySQLMethodSTD1 = "str_to_date('";
    private final String mySQLMethodSTD2 = "', '%Y-%m-%d'), ";
    private final String mySQLMethodSTD3 = "', '%H:%i:%S'), '";
    */

    UseDB(){
        hostName = "localhost";
        portNo = "3306";
        nameDB = "EasyNotes";
        userName = "sa";
        userPswd = "";
    }
    UseDB(String[] arrOfParametr){
        //Сделаем позже
    }

    // Метод connectDB устанавливает соединение с базой
    public void connectBD() {
        File logFile = new File(logFileName);
        String dbConnectUrl ="jdbc:h2:~/DB/"+nameDB;
        System.out.println(dbConnectUrl);

        try {
            Class.forName ("org.h2.Driver") ;
            connect = DriverManager.getConnection(dbConnectUrl, userName, userPswd);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException excSQL) {

            PrintStream ps = null;
            try {
                ps = new PrintStream(logFile);
            } catch (FileNotFoundException eFile) {
                eFile.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Ошибка");
                alert.setHeaderText(null);
                VBox dialogPaneContent = new VBox();
                Label label = new Label("Ошибка при работе с log-файлом: \n");
                TextArea textArea = new TextArea();
                textArea.setText(ExceptionUtils.getStackTrace(eFile));
                dialogPaneContent.getChildren().addAll(label, textArea);
                alert.getDialogPane().setContent(dialogPaneContent);
                Optional<ButtonType> optBtn= alert.showAndWait();
            } finally {
                excSQL.printStackTrace(ps);
                ps.close();
                System.out.println("Problem with DB-connection");
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Ошибка");
                alert.setHeaderText(null);
                alert.setContentText("Ошибка при подключении к базе данных, доп. информация в файле "+logFileName);
                Optional<ButtonType> optBtn= alert.showAndWait();
            }

            //excSQL.printStackTrace(ps);
            System.out.println("Problem with DB-connection");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Ошибка");
            alert.setHeaderText(null);
            alert.setContentText("Ошибка при подключении к базе данных, доп. информация в файле "+logFileName);
            Optional<ButtonType> optBtn= alert.showAndWait();

        }

    }
    public void destroyConnectBD() {
        try {
            connect.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Ошибка");
            alert.setHeaderText(null);
            alert.setContentText("Ошибка при работе с базой данных");

        }
    }


    public boolean checkDB() { //проверка существования БД и правильности структуры, true - если успех

        return true;
    }

    @Override
    public void run() {
        this.connectBD();
    }

    ResultSet getDataFromDB() {
        String sQuery = "SELECT * FROM " + tableName;
        try {

            Statement statement = connect.createStatement();
            /*statement.execute("CREATE DATABASE IF NOT EXISTS "+ nameDB);*/
            statement.execute("CREATE TABLE IF NOT EXISTS notes(idNote int auto_increment, NoteText varchar(100), Date date, Time time, Special varchar(10), primary key(idNote)) ");
            return statement.executeQuery(sQuery);

        } catch (SQLException e) {
            e.printStackTrace();
            errorDetected = true;
            return null;
        }

    }
    public boolean setNewRecord(Note note) {
        try {
            String dataStructure = "(NoteText, Date, Time, Special) ";
            String noteSQLValues = "('" + note.getNoteText() + "', '" + note.getDateOfNote() + "', '" + note.getTimeOfNote() + "', '" + note.getSpecStatus() + "')";
            String insertTableSQL = "INSERT INTO " + tableName + dataStructure + "VALUES" + noteSQLValues;
            System.out.println(insertTableSQL);

            Statement statement = connect.createStatement();
            statement.executeUpdate(insertTableSQL);/**/

            return true;

        } catch (SQLException e) {e.printStackTrace(); errorDetected = true; return false;}
          catch (Exception e1) {e1.printStackTrace(); errorDetected = true; return false;}
    }

    public boolean isErrorDetected() {
        return errorDetected;
    }

    public void setErrorDetected(boolean errorDetected) {
        this.errorDetected = errorDetected;
    }
}
