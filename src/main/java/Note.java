import javax.rmi.CORBA.Tie;
import java.sql.Date;
import java.sql.Time;

public class Note {
    private String noteText;
    private Date dateOfNote;
    private Time timeOfNote;
    private String specStatus;

    Note() {

    }

    Note(String noteText, Date dateOfNote, Time timeOfNote, String specStatus) {
        this.noteText = noteText;
        this.dateOfNote = dateOfNote;
        this.timeOfNote = timeOfNote;
        this.specStatus = specStatus;

    }

    public String getNoteText() {
        return noteText;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }

    public Date getDateOfNote() {
        return dateOfNote;
    }

    public void setDateOfNote(Date dateOfNote) {
        this.dateOfNote = dateOfNote;
    }

    public String getSpecStatus() {
        return specStatus;
    }

    public void setSpecStatus(String specStatus) {
        this.specStatus = specStatus;
    }

    public Time getTimeOfNote() {
        return timeOfNote;
    }

    public void setTimeOfNote(Time timeOfNote) {
        this.timeOfNote = timeOfNote;
    }
}
